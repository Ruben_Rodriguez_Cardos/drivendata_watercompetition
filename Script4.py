import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

def loadData():
    df_train_values = pd.read_csv("Data/TrainValues.csv")

    df_train_labels = pd.read_csv("Data/TrainLabels.csv")

    df_test_values = pd.read_csv("Data/TestValues.csv")

    return df_train_values, df_train_labels,df_test_values

def codeValues2(data,test,colum_to_code, path):

    aux = data[colum_to_code].value_counts().index
    
    dict_of_codes = {}
    code = 1

    for i in aux:
        dict_of_codes[i] = code
        code +=1

    #Almacena el diccionario por si hay que re-codificar
    save = pd.DataFrame.from_dict(dict_of_codes,orient= "index", columns = ['Code'])
    save.to_csv(path)

    values = []
    aux = data[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            values.append(dict_of_codes[i])

    data[colum_to_code] = values
    
    #Tambien se debe codificar test
    values = []
    aux = test[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            if i not in dict_of_codes:
                values.append(0)
            else:
                values.append(dict_of_codes[i])

    test[colum_to_code] = values


    return data, test

def codeValues1(data,colum_to_code, path):

    #print(data)

    aux = data[colum_to_code].value_counts().index
    
    dict_of_codes = {}
    code = 0

    for i in aux:
        dict_of_codes[i] = code
        code +=1

    #Almacena el diccionario por si hay que re-codificar
    save = pd.DataFrame.from_dict(dict_of_codes,orient= "index", columns = ['Code'])
    save.to_csv(path)

    values = []
    aux = data[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            values.append(dict_of_codes[i])

    data[colum_to_code] = values

    return data

def createSubmitFile(predictions,path):
    df_test_values = pd.read_csv("Data/TestValues.csv")

    #decodificar prediciones
    values = []
    decode = pd.read_csv("Code_Meaning/label_code.csv")
    

    dic_decode = {}
    for i in decode.values:
       dic_decode[i[1]] = i[0]


    for i in predictions:
        values.append(dic_decode[i])

    df_test_values["status_group"] = values
    data = df_test_values[["id","status_group"]]
    data.to_csv(path,index=False)


def main():
    data, labels ,test = loadData()

    #Feature selection
    features = ["amount_tsh","installer","population","gps_height","longitude","latitude","region_code",'construction_year']

    
    data = data[features]
    test = test[features]
    labels = labels["status_group"].to_frame()
    

    data,test = codeValues2(data,test,"installer","Code_Meaning/installer_code.csv")
    labels = codeValues1(labels,"status_group","Code_Meaning/label_code.csv")
    
    
    #Tensorflow

    #Crear el modelo
    model = keras.Sequential([
        layers.Dense(64, activation="sigmoid", input_shape=(8,)),
        layers.Dense(64, activation=tf.nn.relu),
        layers.Dense(3)])

    model.compile(optimizer=tf.train.AdamOptimizer(), 
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])


    model.fit(data, labels, epochs=5)
    predictions = model.predict(test)

    predictions = [np.argmax(i) for i in predictions] 

    createSubmitFile(predictions,"Data/Ready_SubmitFile_TF.csv")
    

    
    

if __name__ == "__main__":
    main()