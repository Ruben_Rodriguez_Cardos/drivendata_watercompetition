import pandas as pd
import numpy as np

from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier
from sklearn import preprocessing
from sklearn.decomposition import PCA
from sklearn.covariance import EllipticEnvelope
from sklearn.svm import OneClassSVM

import matplotlib.pyplot as plt

def loadData():
    df_train_values = pd.read_csv("Data/TrainValues.csv")

    df_train_labels = pd.read_csv("Data/TrainLabels.csv")

    #labels = df_train_labels["status_group"].values.tolist()
    #df_train_values["label"] = labels

    df_test_values = pd.read_csv("Data/TestValues.csv")

    return df_train_values, df_train_labels,df_test_values

def createSubmitFile(predictions):
    df_test_values = pd.read_csv("Data/TestValues.csv")

    #decodificar prediciones
    values = []
    decode = pd.read_csv("Code_Meaning/label_code.csv")
    

    dic_decode = {}
    for i in decode.values:
       dic_decode[i[1]] = i[0]


    for i in predictions:
        values.append(dic_decode[i])

    df_test_values["status_group"] = values
    data = df_test_values[["id","status_group"]]
    data.to_csv("Data/Ready_SubmitFile2.csv",index=False)
    
def codeValues1(data,colum_to_code, path):

    #print(data)

    aux = data[colum_to_code].value_counts().index
    
    dict_of_codes = {}
    code = 1

    for i in aux:
        dict_of_codes[i] = code
        code +=1

    #Almacena el diccionario por si hay que re-codificar
    save = pd.DataFrame.from_dict(dict_of_codes,orient= "index", columns = ['Code'])
    save.to_csv(path)

    values = []
    aux = data[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            values.append(dict_of_codes[i])

    data[colum_to_code] = values

    return data

def checkCorrelation(data,labels):
    n_data = data.copy()
    n_data["label"] = labels.values

    #Correlaciones
    df_corr = n_data.corr()
    plt.matshow(df_corr)
    plt.xticks(range(len(df_corr.columns)), df_corr.columns)
    plt.yticks(range(len(df_corr.columns)), df_corr.columns)
    plt.colorbar()
    plt.show()


def doPCA(data,n):
    #Normalización
    min_max_scaler = preprocessing.MinMaxScaler()
    states = min_max_scaler.fit_transform(data)

	#PCA Se explica el 92% de la varización de los datos
    estimator = PCA (n_components = n)
    X_pca = estimator.fit_transform(states)
    print(estimator.explained_variance_ratio_, "Varianza de los datos explicada: ",np.sum(estimator.explained_variance_ratio_), "%")
    return X_pca

def plotByLocation(data,labels):
    n_data =data[["latitude","longitude"]]
    x_1 = []
    y_1 = []

    x_2 = []
    y_2 = []

    x_3 = []
    y_3 = []

    for i,j in zip(n_data.values,labels.values):
        if j == 1:
            x_1.append(i[0])
            y_1.append(i[1])
        if j == 2:
            x_2.append(i[0])
            y_2.append(i[1])
        if j == 3:
            x_3.append(i[0])
            y_3.append(i[1])

    plt.title("Visualización por Posición Geografica")    
    plt.scatter(x_1,y_1 ,alpha=0.5, label='Funcional')
    plt.scatter(x_2,y_2 ,alpha=0.5, label='No Funcional')
    plt.scatter(x_3,y_3 ,alpha=0.5, label='Reparar')
    plt.legend()
    plt.show()


def plotInitialData(X_pca, labels):

    x_1 = []
    y_1 = []

    x_2 = []
    y_2 = []

    x_3 = []
    y_3 = []

    for i,j in zip(X_pca,labels.values):
        if j == 1:
            x_1.append(i[0])
            y_1.append(i[1])
        if j == 2:
            x_2.append(i[0])
            y_2.append(i[1])
        if j == 3:
            x_3.append(i[0])
            y_3.append(i[1])

    plt.title("Visualización PCA ")    
    plt.scatter(x_1,y_1 ,alpha=0.5, label='Funcional')
    plt.scatter(x_2,y_2 ,alpha=0.5, label='No Funcional')
    plt.scatter(x_3,y_3 ,alpha=0.5, label='Reparar')
    plt.legend()
    plt.show()

def checkOutliners(X_pca):
    #Metodo 1
    clf = EllipticEnvelope()
    y_pred = clf.fit(X_pca).predict(X_pca) #Los -1 en y_pred son considerados outliners

    x = []
    y = []

    for i in X_pca:
        x.append(i[0])
        y.append(i[1])

    plt.scatter(x, y, c = y_pred ,alpha=0.5)
    plt.title("Outliers - Metodo 1")
    plt.show()

    #Metodo 2
    clf = OneClassSVM(nu=0.261, gamma=0.05)
    y_pred = clf.fit(X_pca).predict(X_pca) #Los -1 en y_pred son considerados outliners

    plt.title("Outliers - Metodo 2")
    plt.scatter(x, y, c = y_pred ,alpha=0.5)
    plt.show()

def preProcess(data,labels,test):
    #print(data.columns[data.isnull().any()]) #Columnas nulas
    #print(data[data.columns[data.isnull().any()]].isnull().sum()) #Numero de nulos en las columnas con nulos
    
    labels = labels["status_group"].to_frame()
    labels = codeValues1(labels,"status_group","Code_Meaning/label_code.csv")

    #Tecnica 3 - Scikit-learn Label Encoder
    lb_make = LabelEncoder()
    
    cols_to_drop = ["id","wpt_name","num_private","subvillage","region_code","district_code", "lga", "ward", "recorded_by", "scheme_name","scheme_management"]

    data = data.drop(columns = cols_to_drop)

    obj_df = data.select_dtypes(include=['object']).copy()

    #Veo si alguna columna contiene valores nulos (True = hay algun nulo, False = sin nulos)
    cols_with_nan = data.columns[data.isnull().any()]
    data = data.drop(columns = cols_with_nan)
    cols_with_nan = obj_df.columns[obj_df.isnull().any()] 
    obj_df = obj_df.drop(columns = cols_with_nan)

    #Quito las columnas con nulos, como ejemplo, en realidad seria mejor estudiar los nulos, esa columna puede ser importante
    
    cols_encoded = []

    data_columns = data.columns

    for i in obj_df.columns:
        cols_encoded.append(i)
        data[i+"_code"] = lb_make.fit_transform(data[i])

    data = data.drop(columns = cols_encoded)
    
    #print(data.columns)
    #print(data.head(10))


    #Test debe ser codificado igual que train DUDOSO VOY A SUPONER QUE SI
    test = test[data_columns]
    obj_df = test.select_dtypes(include=['object']).copy()
    cols_encoded = []
    for i in obj_df.columns:
        test[i+"_code"] = lb_make.fit_transform(test[i])
        cols_encoded.append(i)
    test = test.drop(columns = cols_encoded)
    #print(test.columns)
    #print(test.head(10))

    return data, labels, test

def doClassification(data, labels, test):
    clf = RandomForestClassifier(n_estimators=1000, random_state=0)
    clf.fit(data, np.ravel(labels.values))
    predictions = clf.predict(test)

    fr=pd.DataFrame({'Attributes': test.columns , 'Random Forests':clf.feature_importances_})
    fr=fr.sort_values(by='Random Forests', ascending=0)
    print(fr)
    return predictions

def main():
    
    #Cargo los datos
    data, labels ,test = loadData()
    
    #Preprocesamiento
    data, labels, test = preProcess(data, labels, test)

    #Estudio inicial de los datos (PCA y visualizacion)
    X_pca = doPCA(data,2)
    plotInitialData(X_pca,labels)
    plotByLocation(data,labels)
    checkOutliners(X_pca)
    checkCorrelation(data,labels)

    #Clasificación
    predictions = doClassification(data, labels, test)    

    createSubmitFile(predictions)
    

if __name__ == "__main__":
    main()