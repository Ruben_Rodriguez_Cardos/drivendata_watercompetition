import pandas as pd
import numpy as np

from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error

import matplotlib.pyplot as plt


def loadData():
    df_train_values = pd.read_csv("Data/TrainValues.csv")

    df_train_labels = pd.read_csv("Data/TrainLabels.csv")

    #labels = df_train_labels["status_group"].values.tolist()
    #df_train_values["label"] = labels

    df_test_values = pd.read_csv("Data/TestValues.csv")

    return df_train_values, df_train_labels,df_test_values

def codeValues2(data,test,colum_to_code, path):

    aux = data[colum_to_code].value_counts().index
    
    dict_of_codes = {}
    code = 1

    for i in aux:
        dict_of_codes[i] = code
        code +=1

    #Almacena el diccionario por si hay que re-codificar
    save = pd.DataFrame.from_dict(dict_of_codes,orient= "index", columns = ['Code'])
    save.to_csv(path)

    values = []
    aux = data[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            values.append(dict_of_codes[i])

    data[colum_to_code] = values
    
    #Tambien se debe codificar test
    values = []
    aux = test[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            if i not in dict_of_codes:
                values.append(0)
            else:
                values.append(dict_of_codes[i])

    test[colum_to_code] = values


    return data, test

def codeValues1(data,colum_to_code, path):

    #print(data)

    aux = data[colum_to_code].value_counts().index
    
    dict_of_codes = {}
    code = 1

    for i in aux:
        dict_of_codes[i] = code
        code +=1

    #Almacena el diccionario por si hay que re-codificar
    save = pd.DataFrame.from_dict(dict_of_codes,orient= "index", columns = ['Code'])
    save.to_csv(path)

    values = []
    aux = data[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            values.append(dict_of_codes[i])

    data[colum_to_code] = values

    return data

def studyRandomTree(data,labels):
    #Divido en train y test
    X_train, X_test, Y_train, Y_test = train_test_split(data,labels,test_size=0.33, random_state=42)

    maes = []

    for n in range(100,150):
        clf = RandomForestClassifier(n_estimators=n, random_state=0)
        clf.fit(X_train, Y_train)
        predictions = clf.predict(X_test)
        mae = mean_absolute_error(Y_test,predictions)
        maes.append(mae)


    #Grafica de los scores
    plt.plot(maes,'b')
    plt.ylabel('Mae')
    plt.xlabel('k - value')
    plt.show()

def createSubmitFile(predictions):
    df_test_values = pd.read_csv("Data/TestValues.csv")

    #decodificar prediciones
    values = []
    decode = pd.read_csv("Code_Meaning/label_code.csv")
    

    dic_decode = {}
    for i in decode.values:
       dic_decode[i[1]] = i[0]


    for i in predictions:
        values.append(dic_decode[i])

    df_test_values["status_group"] = values
    data = df_test_values[["id","status_group"]]
    data.to_csv("Data/Ready_SubmitFile.csv",index=False)


def main():
    data, labels ,test = loadData()

    #Feature selection
    features = ["amount_tsh","installer","population","gps_height","longitude","latitude","region_code",'construction_year']

    
    data = data[features]
    test = test[features]
    labels = labels["status_group"].to_frame()
    

    data,test = codeValues2(data,test,"installer","Code_Meaning/installer_code.csv")
    labels = codeValues1(labels,"status_group","Code_Meaning/label_code.csv")
    
    
    clf = RandomForestClassifier(n_estimators=1000, random_state=0)
    clf.fit(data, np.ravel(labels.values))
    predictions = clf.predict(test)

    fr=pd.DataFrame({'Attributes': features , 'Random Forests':clf.feature_importances_})
    fr=fr.sort_values(by='Random Forests', ascending=0)
    fr.to_csv("Code_Meaning/Random_Forest_Feature_Relevances.csv")
    
    createSubmitFile(predictions)
    

    
    

if __name__ == "__main__":
    main()