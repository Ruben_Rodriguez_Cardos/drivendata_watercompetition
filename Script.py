import pandas as pd
import numpy as np

import tensorflow as tf
from tensorflow import keras

from sklearn.neighbors import KNeighborsClassifier


def loadData():
    df_train_values = pd.read_csv("Data/TrainValues.csv")

    df_train_labels = pd.read_csv("Data/TrainLabels.csv")

    labels = df_train_labels["status_group"].values.tolist()
    df_train_values["label"] = labels

    df_test_values = pd.read_csv("Data/TestValues.csv")

    return df_train_values, df_test_values

def codeValues2(data,test,colum_to_code, path):

    aux = data[colum_to_code].value_counts().index
    
    dict_of_codes = {}
    code = 1

    for i in aux:
        dict_of_codes[i] = code
        code +=1

    #Almacena el diccionario por si hay que re-codificar
    save = pd.DataFrame.from_dict(dict_of_codes,orient= "index", columns = ['Code'])
    save.to_csv(path)

    values = []
    aux = data[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            values.append(dict_of_codes[i])

    data[colum_to_code] = values
    
    #Tambien se debe codificar test
    values = []
    aux = test[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            if i not in dict_of_codes:
                values.append(0)
            else:
                values.append(dict_of_codes[i])

    test[colum_to_code] = values


    return data, test

def codeValues1(data,colum_to_code, path):

    aux = data[colum_to_code].value_counts().index
    
    dict_of_codes = {}
    code = 1

    for i in aux:
        dict_of_codes[i] = code
        code +=1

    #Almacena el diccionario por si hay que re-codificar
    save = pd.DataFrame.from_dict(dict_of_codes,orient= "index", columns = ['Code'])
    save.to_csv(path)

    values = []
    aux = data[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            values.append(dict_of_codes[i])

    data[colum_to_code] = values

    return data


def createSubmitFile(predictions):
    df_test_values = pd.read_csv("Data/TestValues.csv")

    #decodificar prediciones
    values = []
    decode = pd.read_csv("Code_Meaning/label_code.csv")
    

    dic_decode = {}
    for i in decode.values:
       dic_decode[i[1]] = i[0]


    for i in predictions:
        values.append(dic_decode[i])

    df_test_values["status_group"] = values
    data = df_test_values[["id","status_group"]]
    data.to_csv("Data/Ready_SubmitFile.csv",index=False)


def main():
    data, test = loadData()

    #Remove data
    #print(data.columns)
    data = data[["amount_tsh","installer","region_code","label"]]
    test = test[["amount_tsh","installer","region_code"]]
    data,test = codeValues2(data,test,"installer","Code_Meaning/installer_code.csv")
    data = codeValues1(data,"label","Code_Meaning/label_code.csv")
    
    labels = data['label']
    data = data[['amount_tsh','installer','region_code']]


    #Creo modelo keras
    """model = keras.Sequential([
    keras.layers.Dense(64, activation="sigmoid", input_shape=(data.shape[1],)),
    keras.layers.Dense(64, activation=tf.nn.relu),
    keras.layers.Dense(1)])

    optimizer = tf.train.RMSPropOptimizer(0.001)
    
    model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mae'])

    

    EPOCHS = 500

    # Store training stats
    history = model.fit(data, labels, epochs=EPOCHS,
                        validation_split=0.2, verbose=0)


    #Regresión
    predictions = model.predict(test).flatten()
    print(predictions)"""
    neigh = KNeighborsClassifier(n_neighbors=3)
    neigh.fit(data, labels) 
    predictions = neigh.predict(test)
    
    createSubmitFile(predictions)
    

    
    

if __name__ == "__main__":
    main()