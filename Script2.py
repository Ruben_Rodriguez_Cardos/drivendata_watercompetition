import pandas as pd
import numpy as np

import tensorflow as tf
from tensorflow import keras

from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error

import matplotlib.pyplot as plt


def loadData():
    df_train_values = pd.read_csv("Data/TrainValues.csv")

    df_train_labels = pd.read_csv("Data/TrainLabels.csv")

    labels = df_train_labels["status_group"].values.tolist()
    df_train_values["label"] = labels

    df_test_values = pd.read_csv("Data/TestValues.csv")

    return df_train_values, df_test_values

def codeValues2(data,test,colum_to_code, path):

    aux = data[colum_to_code].value_counts().index
    
    dict_of_codes = {}
    code = 1

    for i in aux:
        dict_of_codes[i] = code
        code +=1

    #Almacena el diccionario por si hay que re-codificar
    save = pd.DataFrame.from_dict(dict_of_codes,orient= "index", columns = ['Code'])
    save.to_csv(path)

    values = []
    aux = data[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            values.append(dict_of_codes[i])

    data[colum_to_code] = values
    
    #Tambien se debe codificar test
    values = []
    aux = test[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            if i not in dict_of_codes:
                values.append(0)
            else:
                values.append(dict_of_codes[i])

    test[colum_to_code] = values


    return data, test

def codeValues1(data,colum_to_code, path):

    aux = data[colum_to_code].value_counts().index
    
    dict_of_codes = {}
    code = 1

    for i in aux:
        dict_of_codes[i] = code
        code +=1

    #Almacena el diccionario por si hay que re-codificar
    save = pd.DataFrame.from_dict(dict_of_codes,orient= "index", columns = ['Code'])
    save.to_csv(path)

    values = []
    aux = data[colum_to_code]

    for i in aux:
        if i is np.nan :
            values.append(0)
        else:
            values.append(dict_of_codes[i])

    data[colum_to_code] = values

    return data

def studyKValue(data,labels):
    #Divido en train y test
    X_train, X_test, Y_train, Y_test = train_test_split(data,labels,test_size=0.33, random_state=42)

    maes_uniform = []
    maes_distance = []

    for k in range(2,15):
        for w in ['uniform','distance']:
            neigh = KNeighborsClassifier(n_neighbors=k, weights = w)
            neigh.fit(X_train, Y_train) 
            predictions = neigh.predict(X_test)
            mae = mean_absolute_error(Y_test,predictions)
            if w == 'uniform':
                maes_uniform.append(mae)
            else:
                maes_distance.append(mae)


    #Grafica de los scores
    plt.plot(range(2,15),maes_uniform,'b',label="Uniform")
    plt.plot(range(2,15),maes_distance, 'r',label="Distance")
    plt.ylabel('Mae')
    plt.xlabel('k - value')
    plt.legend()
    plt.show()

def createSubmitFile(predictions):
    df_test_values = pd.read_csv("Data/TestValues.csv")

    #decodificar prediciones
    values = []
    decode = pd.read_csv("Code_Meaning/label_code.csv")
    

    dic_decode = {}
    for i in decode.values:
       dic_decode[i[1]] = i[0]


    for i in predictions:
        values.append(dic_decode[i])

    df_test_values["status_group"] = values
    data = df_test_values[["id","status_group"]]
    data.to_csv("Data/Ready_SubmitFile.csv",index=False)


def main():
    data, test = loadData()

    #Remove data
    #print(data.columns)
    data = data[["amount_tsh","installer","region_code","label"]]
    test = test[["amount_tsh","installer","region_code"]]
    data,test = codeValues2(data,test,"installer","Code_Meaning/installer_code.csv")
    data = codeValues1(data,"label","Code_Meaning/label_code.csv")
    
    labels = data['label']
    data = data[['amount_tsh','installer','region_code']]

    #studyKValue(data,labels) #El mejor resultado se obtiene con Distance y k entre 11 y 14

    neigh = KNeighborsClassifier(n_neighbors=14,weights = 'distance')
    neigh.fit(data, labels) 
    predictions = neigh.predict(test)
    
    createSubmitFile(predictions)
    

    
    

if __name__ == "__main__":
    main()